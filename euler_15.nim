# https://projecteuler.net/problem=15
# Lattice paths
# Problem 15
#
# Starting in the top left corner of a 2×2 grid, and only being able to move to
# the right and down, there are exactly 6 routes to the bottom right corner.
#
# How many such routes are there through a 20×20 grid?

# -> 21 steps, 21 steps!!!

var cache: array[0..20, array[0..20, int]]

var numiter = 0
proc number_of_solutions(n,m:int): int =
    numiter.inc()
    if cache[n][m] > 0:
        result = cache[n][m]
    elif m == 0 or n == 0:
        result = 1
    else:
        result = number_of_solutions(n, m-1) + number_of_solutions(n-1, m)
        cache[n][m] = result

echo number_of_solutions(20, 20)
echo "with ", numiter, " calls"
# 137846528820
# with 801 calls
