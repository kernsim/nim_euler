# https://projecteuler.net/problem=23
# Non-abundant sums
# Problem 23
#
# A perfect number is a number for which the sum of its proper divisors is
# exactly equal to the number. For example, the sum of the proper divisors of 28
# would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
#
# A number n is called deficient if the sum of its proper divisors is less
# than n and it is called abundant if this sum exceeds n.
#
# As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest
# number that can be written as the sum of two abundant numbers is 24. By
# mathematical analysis, it can be shown that all integers greater than 28123
# can be written as the sum of two abundant numbers. However, this upper limit
# cannot be reduced any further by analysis even though it is known that the
# greatest number that cannot be expressed as the sum of two abundant numbers
# is less than this limit.
#
# Find the sum of all the positive integers which cannot be written as the sum
# of two abundant numbers.

import math

proc sum_of_divisors(val: int): int =
    result = 1
    if val == 1: return
    for i in 2..int(sqrt(float(val))):
        if val mod i == 0:
            if i*i < val:
                result += i + int(val/i)
            else:
                result += i

proc isAbundantNumber(number: int): bool =
    number.sum_of_divisors() > number

proc calc_all_abundants(): array[28123+1, bool] =
    for number in 1 .. 28123:
        result[number] = number.isAbundantNumber()

var abundants = calc_all_abundants()

proc is_a_sum(number:int): bool =
    for d in 12 .. number-12:
        if abundants[d] and abundants[number-d]:
            return true
    return false

var sum_of_those_that_are_sums_of_abundant = 0
for number in 1 .. 28123:
    if not is_a_sum(number):
        sum_of_those_that_are_sums_of_abundant += number

echo sum_of_those_that_are_sums_of_abundant

# echo sum_of_divisors(1) # 1
# echo sum_of_divisors(28) # 28
# echo sum_of_divisors(12) # 16
# echo 12.isAbundantNumber()
# echo 28.isAbundantNumber()