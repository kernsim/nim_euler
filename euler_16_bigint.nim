# https://projecteuler.net/problem=16
# Power digit sum
# Problem 16
#
# 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
#
# What is the sum of the digits of the number 2^1000?
import math
import bigint

var value = newBigint(2)
for i in 2 .. 1000:
    value = value * 2
    
echo value
echo value.digits.sum
