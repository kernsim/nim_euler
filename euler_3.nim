# https://projecteuler.net/problem=3
#
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?
import strformat

let
    start_number = int(13195)  # int(600851475143)
var
    number = int(start_number)
    factor = int(2)
    factors: seq[int]

proc is_factor(number: int, candidate:int): bool =
    if number mod candidate == 0:
        return true
    return false


while number > 1  and factor < int(start_number/2):
    if is_factor(number, factor):
        factors.add(factor)
        number = int(number / factor)
    else:
        factor = factor + 1

echo fmt"factors: {factors}"
echo fmt"lagest factor = {factor}"
