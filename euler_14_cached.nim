# https://projecteuler.net/problem=14
# Longest Collatz sequence
#
# The following iterative sequence is defined for the set of positive integers:
#
#    n → n/2 (n is even)
#    n → 3n + 1 (n is odd)
#
# Using the rule above and starting with 13, we generate the following sequence:
# 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

# It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
# Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
#
# Which starting number, under one million, produces the longest chain?
#
# NOTE: Once the chain starts the terms are allowed to go above one million.

iterator collatz(start:int): int =
    assert start >= 1
    var n = start
    while n != 1:
        yield n
        if n mod 2 == 0:
            n = int(float(n) / 2)
        else:
            n = 3 * n + 1
    yield 1


var cache: array[0 .. 999999, int]

proc len_of_collatz(start: int): int =
    result = 0
    for i in collatz(start):
        if i < start:
            result = result + cache[i]
            break
        result.inc()
    cache[start] = result

var max_len = 0

for i in 1 .. 999999:
    var l = len_of_collatz(i)
    if l > max_len:
        max_len = l
        echo i, "  ", max_len

echo " "
echo "max len: ", max_len
