# https://projecteuler.net/problem=9
# Special Pythagorean triplet
#
# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
# a^2 + b^2 = c^2
#
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.
import strformat
import math

for a in 1 .. int(1000/2):
    for b in a+1 .. 999:
        let c = sqrt(pow(float(a), 2) + pow(float(b), 2))
        if float(a)+float(b)+c == 1000.0:
            echo fmt"{a}, {b}, {c}: {a*a}+{b*b} = {c*c}"
            echo fmt"a*b*c = {int(float(a)*float(b)*c)}"
            break
