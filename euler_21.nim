# Amicable numbers
# https://projecteuler.net/problem=21
# Problem 21

# Let d(n) be defined as the sum of proper divisors of n (numbers less than n
# which divide evenly into n).
# If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
# each of a and b are called amicable numbers.

# For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44,
# 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4,
# 71 and 142; so d(284) = 220.

# Evaluate the sum of all the amicable numbers under 10000.
import math

proc sum_of_divisors(val: int): int =
    result = 1
    if val == 1: return
    for i in 2..int(sqrt(float(val))):
        if val mod i == 0:
            if i*i < val:
                result += i + int(val/i)
            else:
                result += i

# proc sum_of_divisors2(val: int): int =
#     result = 1
#     if val > 1:
#         for i in 2..int(float(val)/2):
#             if val mod i == 0:
#                 result += i

var sum_amicable = 0

for number in 0 .. 9999:
    # echo number
    var d1 = sum_of_divisors(number)
    if d1 <= number: # sonst habe ich alle doppelt! Cooler Trick. aus der Lösung :-(
        var d2 = sum_of_divisors(d1)
        if number == d2 and number != d1:
            echo number, " ", d1
            sum_amicable += number + d1

echo "sum_amicable: ", sum_amicable

# echo sum_of_divisors(220)
# echo sum_of_divisors(284)
# echo sum_of_divisors(12285)
# echo sum_of_divisors(14595)

# 284 220
# 1210 1184
# 2924 2620
# 5564 5020
# 6368 6232
# sum_amicable: 31626