# https://projecteuler.net/problem=19
#
# Counting Sundays
#
# You are given the following information, but you may prefer to do some
# research for yourself.
#
#     1 Jan 1900 was a Monday.
#     Thirty days has September,
#     April, June and November.
#     All the rest have thirty-one,
#     Saving February alone,
#     Which has twenty-eight, rain or shine.
#     And on leap years, twenty-nine.
#     A leap year occurs on any year evenly divisible by 4, but not on a
#          century unless it is divisible by 400.
#
# How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?


import strformat
import times

var hits = 0
var d = parse("1901-01-01", "yyyy-MM-dd")
while d.year < 2001:
    if d.monthday == 1 and d.weekday == dSun:
        hits.inc()
    d += 1.days

echo fmt"There are {hits} sundays on the first of a month."
