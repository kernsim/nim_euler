# https://projecteuler.net/problem=7
# 10001st prime
#
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?

var
    primes: seq[int]
    number = 2

proc is_prime(number: int): bool =
    var i = 2
    while (i*i) <= number:
        if number mod i == 0:
            return false
        i += 1
    return true

while primes.len < 10001:
    if number.is_prime():
        primes.add(number)
        # echo len(primes)
    number.inc()

echo primes[^1]
#echo primes
# echo 5.is_prime()
# echo 17.is_prime()
# echo 21.is_prime()
# echo 53.is_prime()
