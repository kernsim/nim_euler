# https://projecteuler.net/problem=6
# Sum square difference
#
# The sum of the squares of the first ten natural numbers is,
# 1^2 + 2^2 + ... + 10^2 = 385

# The square of the sum of the first ten natural numbers is,
# (1 + 2 + ... + 10)^2 = 55^2 = 3025

# Hence the difference between the sum of the squares of the first ten natural numbers
# and the square of the sum is 3025 − 385 = 2640.

# Find the difference between the sum of the squares of the first one hundred natural
# numbers and the square of the sum.

import math
import sequtils
import sugar

proc elementwise_pow(s: seq[int], n: float): seq[int] =
    return s.map(x => int(pow(float(x), n)))

let N = 100
let all = toSeq(1 .. N)
echo sum(all.elementwise_pow(2))
echo int(pow(float(sum(all)), 2))
echo "Diff = ", int(pow(float(sum(all)), 2)) - sum(all.elementwise_pow(2))

# sum = n*(n+1)/2
# sum_of_squares = ...?