# https://projecteuler.net/problem=16
# Power digit sum
# Problem 16
#
# 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
#
# What is the sum of the digits of the number 2^1000?
import math
echo pow(2.0, 100)
echo pow(pow(2.0, 20), 5)


proc flip(row: seq[int]): seq[int] =
    for i, el in row:
        result.add(row[row.len-i-1])


proc add(row: var seq[int], i: int, val:int) =
    row[i] = row[i] + val
    if row[i] < 10:
        return
    let ueb = int(float(row[i])/10)
    row[i] = row[i] mod 10
    row.add(i+1, ueb) 
        

proc multiply(a, b: seq[int]): seq[int] =
    var row : seq[int]
    for i in 0 .. a.len+b.len:
        row.add(0)
    for i, ea in a:
        for j, eb in b:
            row.add(i+j, ea*eb)
    return row

var mat = @[2]
var value = @[2]
for i in 2 .. 1000:
    value = multiply(mat, value)
    # echo i, "  ", value.flip
echo value.flip
echo sum(value)
# var a = @[8, 1, 7].flip
# var b = @[5, 2, 3].flip
# echo multiply(a, b).flip
# @[0, 4, 2, 7, 2, 9, 1]