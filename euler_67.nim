# Maximum path sum I

# By starting at the top of the triangle below and moving to adjacent numbers
# on the row below, the maximum total from top to bottom is 23.
let data1 = """
3
7 4
2 4 6
8 5 9 3"""
#
# That is, 3 + 7 + 4 + 9 = 23.
# Find the maximum total from top to bottom of the triangle below:
    #
    #     NOTE: As there are only 16384 routes, it is possible to solve this
    #           problem by trying every route. However, Problem 67, is the same
    #           challenge with a triangle containing one-hundred rows; it cannot
    #           be solved by brute force, and requires a clever method! ;o)
    
import strutils
import sequtils

let data2 = readFile("p067_triangle.txt")

var values : seq[seq[int]]
for line in data2.split("\n")[0..^2]:
    values.add(line.split().map(parseInt))

var cache: array[0..100, array[0..100, int]]

proc max(a, b:int): int =
    if a > b: result = a
    else:     result = b

var numiter = 0
proc max_sum_over_paths(n,m:int): int =
    numiter.inc()
    if cache[n][m] > 0:
        return cache[n][m]
    result = values[n][m]
    if n < values.len-1:
        result += max(max_sum_over_paths(n+1, m),
                      max_sum_over_paths(n+1, m+1))
    cache[n][m] = result

echo max_sum_over_paths(0, 0)
echo "with ", numiter, " calls"
