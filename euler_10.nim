# https://projecteuler.net/problem=10
# Summation of primes
#
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.

let max_prime = 2000000

var
    primesum: int
    number: int

proc is_prime(number: int): bool =
    if number mod 2 == 0:
        return true
    var i = 3
    while (i*i) <= number:
        if number mod i == 0:
            return false
        i += 2
    return true

primesum = 2
number = 3
while number < max_prime:
    if number.is_prime():
        primesum += number
    number.inc(2)
echo primesum
