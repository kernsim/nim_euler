# Reciprocal cycles

# Problem 26
# A unit fraction contains 1 in the numerator. The decimal representation of the
# unit fractions with denominators 2 to 10 are given:

# 1/2   =   0.5
# 1/3   =   0.(3)
# 1/4   =   0.25
# 1/5   =   0.2
# 1/6   =   0.1(6)
# 1/7   =   0.(142857)
# 1/8   =   0.125
# 1/9   =   0.(1)
# 1/10  =   0.1
# Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be
# seen that 1/7 has a 6-digit recurring cycle.

# Find the value of d < 1000 for which 1/d contains the longest recurring cycle
# in its decimal fraction part.
import sequtils
import strformat

var
    len_max = (0, 0)
    full, rest: int
    last_mods: seq[int]

for d in 2..999:
    last_mods = @[]
    full = int(1/d)
    rest = 1 # 1 mod d
    while rest != 0:
        full = int((rest*10)/d)
        rest = rest*10 mod d
        if (rest in last_mods):
            while (last_mods[0] != rest):
                last_mods.delete(0, 0)
            if last_mods.len > len_max[1]:
                len_max = (d, last_mods.len)
            break
        last_mods.add(rest)

echo fmt"d: {len_max[0]} with length {len_max[1]}"
# d: 983 with length 982

