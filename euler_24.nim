# https://projecteuler.net/problem=24
# Lexicographic permutations
# Problem 24
#
# A permutation is an ordered arrangement of objects. For example, 3124 is one
# possible permutation of the digits 1, 2, 3 and 4. If all of the permutations
# are listed numerically or alphabetically, we call it lexicographic order.
# The lexicographic permutations of 0, 1 and 2 are:

# 012   021   102   120   201   210

# What is the millionth lexicographic permutation of the digits
# 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
import sequtils
import strutils

var count = 0
proc all_permutations(before: seq[int]) =
    if len(before) == 10:
        count.inc()
        if count == 1000000:
            echo count, " ", before.map(proc (x:int):string = $x).join
        return
    block loop:
        for i in 0..9:
            if not (i in before):
                all_permutations(before & @[i])
                if count>1000000: break loop
            
all_permutations(@[])

# 2783915460