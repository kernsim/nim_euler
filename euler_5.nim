# https://projecteuler.net/problem=5
# Smallest multiple
# 
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# 
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

proc is_divisible_by(number: int, factor: int): bool =
    return number mod factor == 0

proc is_divisible_by_2_to_N(number: int, N:int): bool =
    for i in 2..N:
        if not is_divisible_by(number, i):
            return false
    return true

let N = 20
var number = N
while not is_divisible_by_2_to_N(number, 20):
    number = number + N

echo number
