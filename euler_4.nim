# https://projecteuler.net/problem=4
#
# A palindromic number reads the same both ways. The largest palindrome made from
# the product of two 2-digit numbers is 9009 = 91 × 99.
#
# Find the largest palindrome made from the product of two 3-digit numbers.
import unicode
import strformat

proc is_palindrome(word: string): bool =
    return word == word.reversed
    
proc is_palindrome(number: int): bool =
    return is_palindrome($number)
    
# echo "Hallo".is_palindrome()
# echo "HANNAH".is_palindrome()
# echo 999.is_palindrome()
# echo is_palindrome(9939)

var largest_palindrome = 0
var answer = ""
for i in 100..990:
    for j in 100..999:
        let prod = i*j
        if prod.is_palindrome():
            echo fmt"{i} * {j} = {prod}"
            if prod > largest_palindrome:
                largest_palindrome = prod
                answer = fmt"{i} * {j} = {prod}"
echo "largest: ", answer
