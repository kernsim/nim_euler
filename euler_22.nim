# https://projecteuler.net/problem=22
# Names scores
# Problem 22
#
# Using p022_names.txt, a 46K text file containing over five-thousand first
# names, begin by sorting it into alphabetical order. Then working out the
# alphabetical value for each name, multiply this value by its alphabetical
# position in the list to obtain a name score.
#
# For example, when the list is sorted into alphabetical order, COLIN, which is
# worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN
# would obtain a score of 938 × 53 = 49714.
#
# What is the total of all the name scores in the file?
import strutils
import sequtils
import algorithm
import math

proc stripit(x:string):string=
    x.strip(chars={'"'})

proc alphabeticalValue(name:string):int =
    # TEST: echo alphabeticalValue("COLIN") -> 53
    @name.map(proc (x:char):int=int(x)-64).sum

var names = readFile("p022_names.txt").split(",").map(stripit)
sort(names)
var sumOfScores = 0'i64
for i, name in names:
    let position = i + 1
    let alphaVal = alphabeticalValue(name)
    let score =  alphaVal * position
    echo position, " ", name, ": ", alphaVal, " score: ", score
    sumOfScores += int64(score)
echo "sum of scores: ", sumOfScores
# 871198282