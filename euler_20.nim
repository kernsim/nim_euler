# https://projecteuler.net/problem=20
# Factorial digit sum
#
# n! means n × (n − 1) × ... × 3 × 2 × 1
#
# For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
# and the sum of the digits in the
# number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

# Find the sum of the digits in the number 100!
import math
import sequtils
import strutils
proc flip(row: seq[int]): seq[int] =
    for i, el in row:
        result.add(row[row.len-i-1])

proc add(row: var seq[int], i: int, val:int) =
    assert i <= row.len
    if i == row.len:
        row.add(val)
    else:
        row[i] = row[i] + val
    if row[i] >= 10:
        let ueb = int(float(row[i])/10)
        row[i] = row[i] mod 10
        row.add(i+1, ueb)


proc multiply(a, b: seq[int]): seq[int] =
    var row : seq[int]
    for i, ea in a:
        for j, eb in b:
            row.add(i+j, ea*eb)
    return row

proc calcrow2intstring(value: seq[int]): string =
    result = map(value.flip, proc (x:int):string = $x).join


var mat = @[2]
var value = @[1]
for i in 2..100:
    value = multiply(value, @[i])
echo value.calcrow2intstring
echo sum(value)

