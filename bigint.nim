import sequtils
import strutils
import strformat

type
    Bigint* = object
        digits*: seq[int]
        sign*: int


proc `<` * (x, y: Bigint): bool
method `-` * (a, b: Bigint): Bigint {.base.}

proc flip(row: seq[int]): seq[int] =
    for i, el in row:
        result.add(row[row.len-i-1])


proc strip_leading_zeros(a: var seq[int]) =
    while a[^1] == 0:
        a.delete(a.len-1, a.len-1)


proc subtract(a, b: seq[int]): seq[int] =
    var
        da, db: int
        uebertrag = 0
    for i in 0 .. a.len-1:
        # echo "s:", a, " ", b
        if i < b.len:
            db = b[i] + uebertrag
        else:
            db = uebertrag
        da = a[i]
        if da >= db:
            uebertrag = 0
        else:
            da = da + 10
            uebertrag = 1
        result.add(da - db)
        # echo "r: ", result, " u:", uebertrag
    if uebertrag == 1:
        assert false # DARF DAS passieren?
        result.add(uebertrag)
    result.strip_leading_zeros()

proc newBigint*(val: int): Bigint =
    var absval: int
    if val >= 0:
        result.sign = 1
        absval = val
    else:
        result.sign = -1
        absval = -val
    result.digits = absval.`$`.map(proc (x:char):int = parseInt($x)).flip


proc bint * (i: int): Bigint =
    newBigint(i)


method digitsToNumberString(b: Bigint): string {.base.} =
    map(b.digits.flip, proc (x:int):string = $x).join


method `$`*(b: Bigint): string {.base.} =
    var num = b.digitsToNumberString()
    if b.sign > 0:
        return fmt"bint({num})"
    else:
        return fmt"bint(-{num})"


method add_int_at(b:var Bigint, i, val:int) {.base.} =
    assert i <= b.digits.len
    if i == b.digits.len:
        b.digits.add(val)
    else:
        b.digits[i] = b.digits[i] + val
    if b.digits[i] >= 10:
        let ueb = int(float(b.digits[i])/10)
        b.digits[i] = b.digits[i] mod 10
        b.add_int_at(i+1, ueb)




method `*` * (a, b: Bigint): Bigint {.base.} =
    for i, ea in a.digits:
        for j, eb in b.digits:
            result.add_int_at(i+j, ea*eb)
    result.sign = a.sign * b.sign


method `*` * (a: Bigint, ib:int): Bigint {.base.} =
    a * newBigint(ib)


method `+` * (a, b: Bigint): Bigint {.base.} =
    if a.sign == b.sign:
        for i in 0 .. a.digits.len-1:
            add_int_at(result, i, a.digits[i])
        for i in 0 .. b.digits.len-1:
            add_int_at(result, i, b.digits[i])
            result.sign = a.sign
    elif b.sign < 0:
        result = a - b * -1
    elif a.sign < 0:
        result = b - a * -1


method `-` * (a, b: Bigint): Bigint {.base.} =
    if b.sign ==  -1:
        # echo "b is neg"
        result = a + b * -1
    elif a.sign == -1:
        result = (a * -1 + b) * -1
    elif a.sign == 1:
        # echo "beide VZ pos: ", a, " - ", b
        if a < b:
            # echo b , "  ", a, "-> Drehen"
            result = (b - a) * -1
        elif a == b:
            result = newBigint(0)
        else:
            result.digits = subtract(a.digits, b.digits)
            result.sign = 1
            # echo result


proc keys (x, y: Bigint): (string, string) =
    var
        xs = x.digitsToNumberString()
        ys = y.digitsToNumberString()
    if xs.len > ys.len:
        ys = repeat('0', xs.len-ys.len) & ys
    if ys.len > xs.len:
        xs = repeat('0', ys.len-xs.len) & xs
    (xs, ys)

proc `<` * (x, y: Bigint): bool =
    let k = keys(x, y)
    if x.sign == 1 and y.sign == 1:
        result = k[0] < k[1]
    elif x.sign == -1 and y.sign == -1:
        result = k[0] > k[1]
    elif x.sign == 1 and y.sign == -1:
        return false
    elif x.sign == -1 and y.sign == 1:
        return true

proc `<=` * (x, y: Bigint): bool =
    (x < y) or (x == y)

proc `==` * (x, y: Bigint): bool =
    let k = keys(x, y)
    k[0] == k[1] and x.sign == y.sign

proc `==` * (x: Bigint, y:int): bool =
    x == newBigint(y)

method `+` * (a: Bigint, ib:int): Bigint {.base.} =
    a + newBigint(ib)


method `-` * (a: Bigint, ib:int): Bigint {.base.} =
    a - newBigint(ib)


if isMainModule:
    # Test subtract
    proc test_a_minus_b(a, b, solution: int) =
        var res = bint(a) - b
        echo fmt"test: {bint(a)} - {b} = {res}"
        assert(res == solution,
               fmt"a = {a}, b={b}, result = {res}, but {solution} expected")

    proc test_a_plus_b(a, b, solution: int) =
        var res = bint(a) + b
        echo fmt"test: {bint(a)} + {b} = {res}"
        assert(res == solution,
                fmt"a = {a}, b={b}, result = {res}, but {solution} expected")

    test_a_minus_b(10, 12, -2)
    test_a_minus_b(12, 10, 2)
    test_a_minus_b(10, 10, 0)
    test_a_minus_b(10000, 9998, 2)

    test_a_minus_b(-10, -12, 2)
    test_a_minus_b(-12, 10, -22)
    test_a_minus_b(10, -10, 20)
    test_a_minus_b(10000, -9998, 19998)

    test_a_plus_b(10, 12, 22)
    test_a_plus_b(12, 10, 22)
    test_a_plus_b(10, 10, 20)
    test_a_plus_b(10000, 9998, 19998)

    test_a_plus_b(-10, -12, -22)
    test_a_plus_b(-12, 10, -2)
    test_a_plus_b(10, -10, 0)
    test_a_plus_b(10000, -9998, 2)

    # echo newBigint(12345)*newBigint(-6543)
    # echo 12345*6543
    # echo newBigint(12345)-newBigint(-6543)
    # echo newBigint(12345)+6543
    # echo 12345+6543
    # echo newBigint(12345).sign
    # echo newBigint(6543).sign*newBigint(12345).sign
    # echo newBigint(1213)
    # echo newBigint(10) < newBigint(15546346)
    # echo newBigint(10) > newBigint(15546346)
    # echo "f,", newBigint(-15546346) == newBigint(15546346)
    # echo "t,", newBigint(15546346) == newBigint(15546346)
    # echo newBigint(15546346) >= newBigint(15546346)
    # echo newBigint(15546346) <= newBigint(15546346)
    # echo newBigint(15546346-1) <= newBigint(15546346)
    # echo newBigint(15546346-1) >= newBigint(15546346)
    # echo bint(10)+bint(10)
    # echo bint(10)+bint(-10)
    # echo bint(-10)+bint(-10)
    # echo bint(-10)+bint(10)
    # echo bint(10)-10
    # echo bint(10) - -10
    # echo bint(-10)-10
    # echo bint(-10)-bint(-10)
    # echo bint(-534524).digitsToNumberString