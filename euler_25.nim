# https://projecteuler.net/problem=25
# 1000-digit Fibonacci number
# Problem 25
#
# The Fibonacci sequence is defined by the recurrence relation:
#
#     Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
#
# Hence the first 12 terms will be:
#
#     F1 = 1
#     F2 = 1
#     F3 = 2
#     F4 = 3
#     F5 = 5
#     F6 = 8
#     F7 = 13
#     F8 = 21
#     F9 = 34
#     F10 = 55
#     F11 = 89
#     F12 = 144
#
# The 12th term, F12, is the first term to contain three digits.
#
# What is the index of the first term in the Fibonacci sequence to contain
# 1000 digits?

import bigint
import sequtils

var
    f = @[bint(1), bint(1)]
    index = 2

while f[^1].digits.len < 1000:
    f.add(f[0] + f[1])
    f.delete(0, 0)
    index.inc()

echo f[^1]
echo "Rank: ", index