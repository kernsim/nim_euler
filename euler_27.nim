# https://projecteuler.net/problem=27
# Quadratic primes
# Problem 27
#
# Euler discovered the remarkable quadratic formula:

#     n^2 + n + 41

# It turns out that the formula will produce 40 primes for the consecutive
# integer values 0 ≤ n ≤ 39. However, when n=40,40^2+40+41=40(40+1)+41 is
# divisible by 41, and certainly when n=41,41^2+41+41 is clearly divisible
# by 41.

# The incredible formula n^2 − 79n + 1601
# was discovered, which produces 80 primes for the consecutive values 0≤n≤79.
# The product of the coefficients, −79 and 1601, is −126479.

# Considering quadratics of the form:

#     n^2 + a n + b

# , where |a|<1000 and |b|≤1000

# where |n| is the modulus/absolute value of n
# e.g. |11|=11 and |−4|=4

# Find the product of the coefficients, a and b, for the quadratic expression
# that produces the maximum number of primes for consecutive values of n,
# starting with n=0.

import strformat
import math

proc isAPrime(val: int): bool =
    # Based on "sum of divisors", exclude 1 and abort asap
    if val < 2:
        return false
    if val == 2:
        return true
    if val mod 2 == 0:
        return false
    var i = 3
    while i <= int(sqrt(float(val))):
        if val mod i == 0:
            return false
        i += 2
    return true

# for i in 0..99:
    # if i.isAPrime: i.echo

proc eq(a, b, n: int): int =
    n*n + a*n + b

var
    n_max = -1
    a_max = -99999
    b_max = -99999
    
for a in -999..999:
    for b in -999..999:
        var n = 0
        while eq(a, b, n).isAPrime:
            n.inc()
        if (n-1 > n_max):
            n_max = n - 1
            a_max = a
            b_max = b

echo fmt"max length: {n_max} at a={a_max} and b={b_max}"
echo fmt"a*b = {a_max*b_max}"
