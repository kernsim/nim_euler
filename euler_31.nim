# https://projecteuler.net/problem=31
# Coin sums
# Problem 31
#
# In England the currency is made up of pound, £, and pence, p, and there are eight coins in
# general circulation:
#
#     1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
#
# It is possible to make £2 in the following way:
#
#     1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
#
# How many different ways can £2 be made using any number of coins?

import strformat

let all_coins = @[200, 100, 50, 20, 10, 5, 2, 1]

proc number_of_possibilities(remaining_amount: int, remaining_coins: seq[int]): int =
    if len(remaining_coins) == 1:
        result = 1
    else:
        let coin = remaining_coins[0]
        let number_of_coins = int(remaining_amount/coin)
        result = 0
        for i in 0..number_of_coins:
            result += number_of_possibilities(remaining_amount-i*coin,
                                              remaining_coins[1.. ^1])
    # echo fmt"number_of_possibilities({remaining_amount}, {remaining_coins}) = {result}"


let amount = 200
let number = number_of_possibilities(amount, all_coins)

echo fmt"There are {number} possibilites to make {amount}p out of coins"
