# https://projecteuler.net/problem=30
# Digit fifth powers
# Problem 30

# Surprisingly there are only three numbers that can be written as the sum of fourth powers of
# their digits:

#     1634 = 1^4 + 6^4 + 3^4 + 4^4
#     8208 = 8^4 + 2^4 + 0^4 + 8^4
#     9474 = 9^4 + 4^4 + 7^4 + 4^4

# As 1 = 1^4 is not a sum it is not included.
# The sum of these numbers is 1634 + 8208 + 9474 = 19316.
# 
# Find the sum of all the numbers that can be written as the sum of fifth powers of their
# digits.

import strformat
import math
var sum_of_all = 0

proc p(x, y: int): int =
    int(pow(float(x), float(y)))

proc p5(x: int): int =
    int(pow(float(x), 5.0))
    
proc e(y: int): int =
    int(pow(10.0, float(y)))
    
for i in 0..9:
    echo fmt"{i}^5 = {p(i, 5)}"

var res: seq[int]
for a6 in 0..9:
    for a5 in 0..9:
        for a4 in 0..9:
            for a3 in 0..9:
                for a2 in 0..9:
                    for a1 in 0..9:
                        for a0 in 0..9:
                            var number = a6*e(6)+a5*e(5)+a4*e(4)+a3*e(3)+a2*e(2)+a1*e(1)+a0
                            var sp = p5(a6)+p5(a5)+p5(a4)+p5(a3)+p5(a2)+p5(a1)+p5(a0)
                            if sp == number:
                                echo fmt"{a6}+{a5}, {a4}, {a3}, {a2}, {a1}, {a0} = {number} -> {sp}"
                            if number == sp:
                                res.add(sp)

echo res
echo res[2 .. ^1]
echo sum(res[2 .. ^1])