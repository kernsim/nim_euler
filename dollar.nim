type
    Dollar* = distinct float

proc `$`*(dollar: Dollar): string =
    return $float(dollar) & " $"

proc `+`*(a, b:Dollar): Dollar {.borrow.}

var d = Dollar(1000)
var d2 = 1000.2.Dollar

echo d
echo d+d2
# echo d+1.5 # ERROR
